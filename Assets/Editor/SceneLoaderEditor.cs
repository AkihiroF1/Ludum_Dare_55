using UnityEditor;
using UnityEngine;
using Utils;

namespace Editor
{
    [CustomEditor(typeof(SceneLoader), true)]
    public class SceneLoaderEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            SceneLoader myTarget = (SceneLoader)target;

            // Получаем список сцен
            string[] scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes);

            // Создаем массив имен сцен для выпадающего списка
            string[] sceneNames = new string[scenes.Length];
            for (int i = 0; i < scenes.Length; i++)
            {
                sceneNames[i] = System.IO.Path.GetFileNameWithoutExtension(scenes[i]);
            }

            // Определяем текущий индекс выбранной сцены
            int currentIndex = Mathf.Max(System.Array.IndexOf(sceneNames, myTarget.NameSceneLoading), 0);

            // Создаем выпадающий список
            currentIndex = EditorGUILayout.Popup("Scene Loading", currentIndex, sceneNames);

            // Обновляем имя сцены в зависимости от выбора пользователя
            myTarget.NameSceneLoading = sceneNames[currentIndex];

            // Сохраняем изменения
            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }

            DrawDefaultInspector();
        }
    }

}