using InteractionSystem;
using Player;
using UnityEngine.InputSystem;
using Zenject;

namespace Input
{
    public class InputHandler
    {
        private InteractComponent _interactComponent;
        private PlayerHands _playerHands;

        [Inject]
        InputHandler(InteractComponent interactComponent, PlayerHands playerHands) 
        {
            _interactComponent = interactComponent;
            _playerHands = playerHands;
        }

        public void EnterDialogue(InputAction.CallbackContext obj)
        {
            _interactComponent.OnInteractable();
        }

        public void DropItem(InputAction.CallbackContext obj)
        {
            _playerHands.DropObject();
        }
    }
}