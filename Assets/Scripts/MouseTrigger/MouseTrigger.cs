using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject firstObjectToActivate;
    [SerializeField] private GameObject secondObjectToActivate;

    public void OnPointerEnter(PointerEventData eventData)
    {
        firstObjectToActivate.SetActive(true);
        secondObjectToActivate.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        firstObjectToActivate.SetActive(false);
        secondObjectToActivate.SetActive(false);
    }
}
