using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utils
{
    public class SceneLoader : MonoBehaviour
    {
        [HideInInspector] public string NameSceneLoading;

        public void LoadScene()
        {
            SceneManager.LoadScene(NameSceneLoading);
        }
    }
}