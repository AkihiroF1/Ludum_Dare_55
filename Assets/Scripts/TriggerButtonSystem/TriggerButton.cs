using DoorSystem;
using UnityEngine;
using Utils;

namespace TriggerButtonSystem
{
    public class TriggerButton : MonoBehaviour
    {
        [SerializeField] private Door door;
        [SerializeField] private LayerMask layerMask;
        
    
        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (layerMask.CheckLayer(collider.gameObject.layer))
            {
                door.OpenDoor();
            }
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (layerMask.CheckLayer(collider.gameObject.layer))
            {
                door.OpenDoor(false);
            }
        }
    }
}
