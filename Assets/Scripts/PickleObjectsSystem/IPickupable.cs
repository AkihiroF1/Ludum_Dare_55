using InteractionSystem;

namespace PickleObjectsSystem
{
    public interface IPickupable : IInteractable
    {
        void Drop();
    }
}