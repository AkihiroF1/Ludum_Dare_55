using InteractionSystem.InteractingObject;
using UnityEngine;
using Utils;

namespace PickleObjectsSystem
{
    public class MovingInteractingObject : MonoBehaviour, IPickupable
    {
        [SerializeField] private LayerMask interactingObjectLayer;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip dropKeySound;
        public void Interact()
        {
            if (interactingObjectLayer.CheckLayer(8))
            {
                audioSource.PlayOneShot(dropKeySound);
            }
        }

        public void Drop()
        {
            
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (interactingObjectLayer.CheckLayer(collision.gameObject.layer))
            {
                if (collision.gameObject.TryGetComponent(out IInteractingObject interactingObject))
                {
                    interactingObject.InteractingWithObject();
                }
            }
        }
    }
}