using UnityEngine;
using Utils;

namespace PickleObjectsSystem
{
    public class Board : MonoBehaviour, IPickupable
    {
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private LayerMask waterLayer;
        [SerializeField] private Collider2D riverCollider; // Коллайдер річки
        [SerializeField] private Collider2D boardCollider; // Коллайдер доски

        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip dropBoardSound;

        private bool _isPlaced = false; // Чи доска укладена
        private bool _isInWater; //Чи доска у воді
        public void Interact()
        {
            _isPlaced = false; // Доска піднята
            boardCollider.enabled = false; // Вимкнення колайдера доски
        }

        public void Drop()
        {
            audioSource.PlayOneShot(dropBoardSound);
            _isPlaced = true; // Доска укладена
            if(!_isInWater)
                boardCollider.enabled = true; // Активація колайдера доски
            ToggleRiverCollider(false); // Вимкнення колайдера річки, якщо доска укладена на річку
        }
        private void ToggleRiverCollider(bool isEnabled = true)
        {
            riverCollider.enabled = isEnabled;
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (playerLayer.CheckLayer(other.gameObject.layer) && _isPlaced && _isInWater)
            {
                ToggleRiverCollider(false);
            }
            if (waterLayer.CheckLayer(other.gameObject.layer))
            {
                _isInWater = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (playerLayer.CheckLayer(other.gameObject.layer) && _isPlaced)
            {
                ToggleRiverCollider();
            }
            if (waterLayer.CheckLayer(other.gameObject.layer) && !_isPlaced)
            {
                _isInWater = false;
            }
        }
    }
}