using InteractionSystem;
using UnityEngine;

namespace DialogueSystem.Activatiors
{
    public class DialogueInteractActivator : MonoBehaviour, IInteractable
    {
        [SerializeField] private DialogueSwitcher targer;
        public void Interact()
        {
            targer.StartDialogue();
        }
    }
}