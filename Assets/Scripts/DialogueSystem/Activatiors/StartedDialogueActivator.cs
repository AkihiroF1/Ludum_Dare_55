using UnityEngine;

namespace DialogueSystem.Activatiors
{
    public class StartedDialogueActivator : MonoBehaviour
    {
        [SerializeField] private DialogueSwitcher target;

        private void Start()
        {
            target.StartDialogue();
        }
    }
}