using deVoid.Utils;
using DialogueSystem.Data;
using Events;
using UnityEngine;
using UnityEngine.Events;

namespace DialogueSystem
{
    public class DialogueSwitcher : MonoBehaviour
    {
        [SerializeField] private DialogueSequenceSO dialogueSequenceSo;
        [SerializeField] private DialogueView dialogueView;
        [SerializeField] private bool isWayDialogue;
        
        [SerializeField] private UnityEvent onDialogueComplete; // UnityEvent для реакції на завершення діалогу
        
        private Dialogue _currentDialogue;
        

        public void StartDialogue()
        {
            _currentDialogue = dialogueSequenceSo.Dialogues[0];
            if (_currentDialogue != null)
            {
                dialogueView.EnableDialogue(this);
                dialogueView.UpdateDialogueUI(_currentDialogue.line, _currentDialogue.speakerIcon);
                Signals.Get<OnEnableMovement>().Dispatch(false);
            }
        }

        public void ProceedToNextDialogue()
        {
            Dialogue nextDialogue = dialogueSequenceSo.GetNextDialogue(_currentDialogue.id);
            if (nextDialogue != null)
            {
                _currentDialogue = nextDialogue;
                dialogueView.UpdateDialogueUI(_currentDialogue.line, _currentDialogue.speakerIcon);
            }
            else
            {
                EndDialogue();
            }
        }

        public void EndDialogue()
        {
            if(!isWayDialogue)
                Signals.Get<OnEnableMovement>().Dispatch(true);
            
            dialogueView.HideDialogueUI();
            
            onDialogueComplete.Invoke();
            
            // Тут можна додати інші дії після завершення діалогу
        }
    }
}