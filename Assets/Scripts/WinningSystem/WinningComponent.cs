using UnityEngine;

namespace WinningSystem
{
    public class WinningComponent : MonoBehaviour
    {
        private const int MaxCountBadChooses = 2;
        [SerializeField] private CanvasGroup winPanel;
        [SerializeField] private CanvasGroup losePanel;

        private int _countBadChooses;

        public void AddBadChoose() => _countBadChooses++;

        public void EnableFinishPanel()
        {
            var currentPanel = ProceedChooses();

            currentPanel.alpha = 1;
            currentPanel.interactable = true;
            currentPanel.blocksRaycasts = true;
        }

        private CanvasGroup ProceedChooses()
             => _countBadChooses >= MaxCountBadChooses ? losePanel : winPanel;
    }
}