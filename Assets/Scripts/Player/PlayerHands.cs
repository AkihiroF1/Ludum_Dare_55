using PickleObjectsSystem;
using UnityEngine;

namespace Player
{
    public class PlayerHands : MonoBehaviour
    {
        [SerializeField] private Transform handPosition;
        [SerializeField] private GameObject dropInteractPanel;
        private GameObject _currentHeldObject;
        private IPickupable _pickupableObject;

        public void PickUpObject(GameObject objectToPickUp, IPickupable pickupableObject)
        {
            if (_currentHeldObject != null)
                return;
            _pickupableObject = pickupableObject;
            dropInteractPanel.SetActive(true);
            _currentHeldObject = objectToPickUp;
            _currentHeldObject.transform.SetParent(handPosition);
            _currentHeldObject.transform.localPosition = Vector3.zero;
            _currentHeldObject.transform.localRotation = Quaternion.identity;
        }

        public void DropObject()
        {
            if (_currentHeldObject == null)
                return;

            _currentHeldObject.transform.SetParent(null);
            _currentHeldObject = null;
            dropInteractPanel.SetActive(false);
            _pickupableObject.Drop();
            _pickupableObject = null;
        }
    }
}