using System.Collections;
using UnityEngine;
using Zenject;

namespace Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private Transform handTransform;
        [SerializeField] private GameObject initialPanel; // Панель, яка показується при появі на сцені
        [SerializeField] private float durationTutorial;
        [SerializeField] private float speedMoving; // швидкість руху персонажа
        private Rigidbody2D _rb;
        [Inject] private PlayerInput _input;

        private bool _hasMoved = false; // Чи рухався гравець

        private void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            initialPanel.SetActive(true); // Активувати панель при старті
        }

        private void FixedUpdate()
        {
            Vector2 movementDirection = _input.Player.Moving.ReadValue<Vector2>();
            _rb.velocity = movementDirection * speedMoving;
            
            animator.SetFloat("Direction", movementDirection.x);
            if (movementDirection == Vector2.zero)
                return;
            
            RotateTowardsMovementDirection(movementDirection);
            
            if (_hasMoved) 
                return;
                
            _hasMoved = true; // Гравець почав рух
            StartCoroutine(HideInitialPanel());
        }

        private void RotateTowardsMovementDirection(Vector2 direction)
        {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            handTransform.rotation = Quaternion.Euler(0, 0, angle);
        }

        private IEnumerator HideInitialPanel()
        {
            yield return new WaitForSeconds(durationTutorial); // Зачекати кілька секунд перед хованням панелі
            initialPanel.SetActive(false); // Зробити панель неактивною
        }
    }
}