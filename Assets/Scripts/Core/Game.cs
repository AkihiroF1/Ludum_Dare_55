using deVoid.Utils;
using Events;
using Input;
using Zenject;

namespace Core
{
    public class Game
    {
        private readonly PlayerInput _playerInput;
        private readonly InputHandler _inputHandler;

        [Inject]
        public Game(PlayerInput playerInput, InputHandler inputHandler)
        {
            _playerInput = playerInput;
            _inputHandler = inputHandler;
            Subscribe();
        }

        public void StartGame()
        {
            EnableMovement(true);
        }

        public void Bind()
        {
            _playerInput.Player.Interactive.performed += _inputHandler.EnterDialogue;
            _playerInput.Player.DropItem.performed += _inputHandler.DropItem;
        }

        private void Subscribe()
        {
            Signals.Get<OnEnableMovement>().AddListener(EnableMovement);
        }

        public void Unsubscribe()
        {
            Signals.Get<OnEnableMovement>().RemoveListener(EnableMovement);
        }

        private void EnableMovement(bool isEnable)
        {
            if (isEnable)
            {
                _playerInput.Player.Enable();
            }
            else 
            {
                _playerInput.Player.Disable();
            }
        }
    }
}