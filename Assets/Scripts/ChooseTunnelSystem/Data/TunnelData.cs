using System;
using UnityEngine;
using UnityEngine.UI;

namespace ChooseTunnelSystem.Data
{
    [Serializable]
    public struct TunnelData
    {
        public Button button;
        public Transform pointTeleport;
        public Transform pointTunnel;
        public bool isGoodWay;
    }
}