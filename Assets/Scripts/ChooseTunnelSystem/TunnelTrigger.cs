using UnityEngine;
using Utils;

namespace ChooseTunnelSystem
{
    public class TunnelTrigger : MonoBehaviour
    {
        [SerializeField] private TunnelChooser tunnelChooser;
        [SerializeField] private LayerMask layer;
        private void OnTriggerEnter2D(Collider2D otherCollider)
        {
            if (!layer.CheckLayer(otherCollider.gameObject.layer)) 
                return;
            
            tunnelChooser.StartChooseTunnel();
            gameObject.SetActive(false);
        }
    }
}