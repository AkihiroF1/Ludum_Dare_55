using ChooseTunnelSystem.Data;
using deVoid.Utils;
using DG.Tweening;
using DialogueSystem;
using Events;
using UnityEngine;
using UnityEngine.Events;
using WinningSystem;

namespace ChooseTunnelSystem
{
    public class TunnelChooser : MonoBehaviour
{
    
    [SerializeField] private TunnelData[] tunnelDatas;
    [SerializeField] private WinningComponent winningComponent;
    [SerializeField] private Transform player;
    [SerializeField] private DialogueSwitcher dialogueSwitcher;
    [SerializeField] private CanvasGroup darkScreen;
    [SerializeField] private CanvasGroup choosePanel;
    [SerializeField] private float moveSpeedPlayerToTunnel = 5.0f; // Швидкість руху гравця до точки тунелю
    [SerializeField] private float durationEnabledBlackScreen = 0.5f; // Швидкість руху гравця до точки тунелю

    [SerializeField] private UnityEvent onDialogueComplete; // UnityEvent для реакції на завершення діалогу
    public void StartChooseTunnel()
    {
        foreach (var data in tunnelDatas)
        {
            data.button.onClick.AddListener(() => StartTunnelTransition(data));
        }
        dialogueSwitcher.StartDialogue(); // Початок діалогу
        EnableChoosePanel();
    }

    private void EnableChoosePanel(bool isEnable = true)
    {
        choosePanel.alpha = isEnable ? 1 : 0; // Зробити панель вибору видимою
        choosePanel.blocksRaycasts = isEnable;// Дозволити взаємодію з панеллю
        choosePanel.interactable = isEnable;
    }

    private void StartTunnelTransition(TunnelData tunnelData)
    {
        if(!tunnelData.isGoodWay)
            winningComponent.AddBadChoose();
        
        dialogueSwitcher.EndDialogue();
        EnableChoosePanel(false);
        ClearButtonData(); // Очистка подій кнопок
        var position = tunnelData.pointTunnel.position;
        player.DOMove(position, Vector3.Distance(player.position, position) / moveSpeedPlayerToTunnel).SetEase(Ease.Linear).OnComplete(() => 
        {
            // Плавне переміщення гравця до заданої точки
            darkScreen.DOFade(1, durationEnabledBlackScreen).SetEase(Ease.OutQuad).OnComplete(() =>
            {
                PerformTeleportation(tunnelData.pointTeleport.position); // Телепортація після досягнення цілі
            });
        });
    }

    private void PerformTeleportation(Vector3 newPosition)
    {
        onDialogueComplete.Invoke();
        player.position = newPosition; // Зміна позиції гравця на нову
        // Плавне розсвітлення екрану
        darkScreen.DOFade(0, durationEnabledBlackScreen).SetEase(Ease.InQuad);
        Signals.Get<OnEnableMovement>().Dispatch(true);
    }

    private void ClearButtonData()
    {
        foreach (var data in tunnelDatas)
        {
            data.button.onClick.RemoveAllListeners(); // Видалення усіх слухачів подій для кнопок
        }
    }
}
}