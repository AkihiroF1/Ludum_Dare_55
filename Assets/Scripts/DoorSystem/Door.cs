using UnityEngine;

namespace DoorSystem
{
    public class Door : MonoBehaviour
    {
        [SerializeField] private Collider2D doorCollider;
        [SerializeField] private SpriteRenderer spriteRender;

        [SerializeField] private Sprite openDoorSprite;
        [SerializeField] private Sprite closeDoorSprite;

        [SerializeField] private AudioClip OpenSound;
        [SerializeField] private AudioSource audioSource;

        public void OpenDoor(bool isOpen = true)
        {
            doorCollider.enabled = !isOpen;
            spriteRender.sprite = isOpen ? openDoorSprite : closeDoorSprite;
            audioSource.PlayOneShot(OpenSound);
        }
    }
}
