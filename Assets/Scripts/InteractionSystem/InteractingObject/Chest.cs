using UnityEngine;

namespace InteractionSystem.InteractingObject
{
    public class Chest : MonoBehaviour, IInteractingObject
    {
        [SerializeField] private GameObject objectInside;
        [SerializeField] private SpriteRenderer spriteRender;
        [SerializeField] private Sprite openChestSprite;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip openChestSound;

        private void Awake()
        {
            objectInside.SetActive(false);
        }
        public void InteractingWithObject()
        {
            spriteRender.sprite = openChestSprite;
            objectInside.SetActive(true);
            audioSource.PlayOneShot(openChestSound);
        }
    }
}
