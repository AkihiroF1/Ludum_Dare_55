﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace InteractionSystem.InteractingObject
{
    public class Wall : MonoBehaviour, IInteractingObject
    {
        [SerializeField] public AudioSource audioSource;
        [SerializeField] public AudioClip brokeWall;
        private bool isPlaying = false;

        public void InteractingWithObject()
        {
            audioSource.PlayOneShot(brokeWall);
            StartCoroutine(DestroyWithDelay());
        }

        IEnumerator DestroyWithDelay()
        {
            isPlaying = true;
            yield return new WaitForSeconds(0.5f);
            DestroyWall();
            isPlaying = false;
        }

        private void DestroyWall()
        {
            gameObject.SetActive(false);
        }
    }
}
