using PickleObjectsSystem;
using Player;
using UnityEngine;
using Utils;

namespace InteractionSystem
{
    public class InteractComponent : MonoBehaviour
    {
        [SerializeField] private LayerMask interactLayer;
        [SerializeField] private float triggerZoneRadius;
        [SerializeField] private GameObject interactPanel;
        [SerializeField] private CircleCollider2D triggerCollider;
        [SerializeField] private PlayerHands handsComponent;

        private void Awake()
        {
            triggerCollider.radius = triggerZoneRadius;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (interactLayer.CheckLayer(other.gameObject.layer))
                interactPanel.SetActive(true);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (interactLayer.CheckLayer(other.gameObject.layer))
                interactPanel.SetActive(false);
        }

        public void OnInteractable()
        {
            Collider2D otherCollider = Physics2D.OverlapCircle(transform.position, triggerZoneRadius, interactLayer);
            if (otherCollider != null)
            {
                if (otherCollider.TryGetComponent(out IInteractable interactable))
                {
                    interactPanel.SetActive(false);
                    interactable.Interact();
                    if (interactable is IPickupable pickupable)
                    {
                        handsComponent.PickUpObject(otherCollider.gameObject, pickupable);
                    }
                }
            }
        }
    }
}